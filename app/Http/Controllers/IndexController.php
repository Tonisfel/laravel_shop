<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateRequest;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function create(CreateRequest $request){
        $product = new Product();

        $product->name = $request->input("product-name");
        $product->price = $request->input("product-price");
        $product->save();
        return redirect()->route("home")->with("success", "Product has been added to database successfully!");
    }

    public function allData(){
        $products = DB::select("SELECT `products`.`id`,
                                              `products`.`name`,
                                              `products`.`price`,
                                               count(`pictures`.`id`) AS pic_count
                                       FROM `products`
                                       LEFT JOIN pictures ON `products`.`id` = `pictures`.`product_id`
                                       GROUP BY `id`,`name`,`price`
                                    ");
        return view("index", ["data" => $products]);
    }

    public function edit(Request $request, $id) {
        $resultCustom = DB::select("SELECT `name`,`price` FROM `products` WHERE `id` = $id");
        $resultUrls = DB::select("SELECT `id`, `url` FROM `pictures` WHERE `product_id` = $id");
        return view("edit", ["custominfo" => $resultCustom,
                                  "urls" => $resultUrls,
                                  "id" => $id ]);
    }

    public function save(Request $request){
        DB::update("UPDATE `products` SET `name` = ?, `price` = ? WHERE `id` = ?", [$request->input("product-name"),
                                                                            $request->input("product-price"),
                                                                            $request->input("product-id")]);

        for ($i = 1; $i <= $request->input("product-pictures-count"); $i++)
            DB::insert($this->buildSQLInsertPics($request->input("product-id-$i"),
                                                 $request->input("product-id"),
                                                 $request->input("product-url-$i")));
        return redirect()->route("home")->with("success", "Product information has been updated!");
    }

    public function remove(Request $request, $id){
        DB::delete("DELETE FROM `products` WHERE `id` = ?", [$id]);
        return redirect()->route("home")->with("success", "Product has been deleted.");
    }

    public function deletepic(Request $request, $id){
        DB::delete("DELETE FROM `pictures` WHERE `id` = ?", [$id]);
    }

    private function buildSQLInsertPics($pic_id = null, $id = null, $url = ""){
        if ($pic_id === null){
            $sqlQuery = "INSERT INTO `pictures` (`id`, `product_id`, `url`)
                         VALUES (NULL, $id, '$url')
                         ON DUPLICATE KEY UPDATE `url` = '$url'";
        } else {
            $sqlQuery = "INSERT INTO `pictures` (`id`, `product_id`, `url`)
                         VALUES ($pic_id, $id, '$url')
                         ON DUPLICATE KEY UPDATE `url` = '$url'";
        }
        return $sqlQuery;
    }
}
