@extends("layouts.app")

@section("title-page") New product @endsection

@section('content')
    <h1>Creating product</h1>
    <p>Form for creating product.</p>
    <div class="alert alert-info">
        <span class="glyphicon glyphicon-info-sing"></span>
        Name must be longer then 4 and price cannot be empty or negative.
    </div>

    @include("inc.messages")

    <form method="post" action="{{ route("add-form") }}">
        @csrf
        <div class="form-group">
            <label for="product-name">Product name:</label>
            <input class="form-control" type="text" id="product-name" name="product-name">
        </div>
        <div class="form-group">
            <label for="product-price">Product price:</label>
            <input class="form-control" type="text" id="product-price" name="product-price">
        </div>
        <hr>
        <button class="btn btn-default" type="submit">Create new product</button>
    </form>
@endsection
