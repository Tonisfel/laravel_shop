@extends("layouts.app")

@section("title-page") Edit product @endsection

@section('content')
    <h1>Edit product</h1>
    <p>Here you can edit a product.</p>
    <hr>
    <div class="container">
        <form method="post" action="{{ route("save-form") }}">
            @csrf
            <input type="hidden" value="{{ $id }}" name="product-id">
            <label for="product-name">Product name:</label>
            <input class="form-control" id="product-name" name="product-name" type="text" placeholder="Type a name of new product" value="{{ $custominfo[0]->name}}">
            <br>
            <label for="product-coast">Product price:</label>
            <input class="form-control" id="product-price" name="product-price" type="text" placeholder="Type a price of new product" value="{{ $custominfo[0]->price }}">
            <br>
            <label for="product-pictures-count">Product pictures count:</label>
            <div class="input-group">
                <input class="form-control" id="product-pictures-count" name="product-pictures-count" type="text" value="{{ count($urls) }}" readonly>
                <div class="input-group-btn">
                    <button class="btn btn-default" id="product-pic-count-increase-btn" type="button">+</button>
                </div>
            </div>
            <br>
            <div id="product-count-urls-div">
                @for($i = 0; $i < count($urls); $i++)
                <div id="product-url-{{ $i+1 }}" class="input-group">
                    <input type="hidden" name="product-id-{{ $i+1 }}" value="{{ $urls[$i]->id }}">
                    <input type="text" name="product-url-{{ $i+1 }}" class="form-control" value="{{ $urls[$i]->url }}">
                    <div class="input-group-btn">
                        <button name="product-remove-btn" type="button" class="btn btn-default" id="product-remove-btn-{{ $i+1 }}" onclick="removeOnClick($(this));">Remove</button>
                    </div>
                </div>
                @endfor
            </div>
            <hr>
            <button class="btn btn-default" type="submit" name="product-create">Edit</button>
        </form>
    </div>
    <script type="text/javascript" src="/sources/js/editformjs.js"></script>
@endsection
