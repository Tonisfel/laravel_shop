@extends("layouts.app")

@section("title-page") Home @endsection

@section('content')
    <h1>Welcome!</h1>
    <p>Welcome to the shop!</p>
    <hr>
    @include("inc.messages")
    <p><strong>Product list:</strong></p>
    <table class="table">
        <thead>
            <tr>
                <td>Name</td>
                <td>Price</td>
                <td>Pictures</td>
                <td></td>
                <td></td>
            </tr>
        </thead>
        <tbody>
            @if (count($data) == 0)
                <tr>
                   <td colspan="5" style="text-align: center">
                       <span class="glyphicon glyphicon-info-sign"></span> No products in shop.
                   </td>
                </tr>
            @else
                @foreach ($data as $el)
                <tr>
                    <td>{{ $el->name }}</td>
                    <td>{{ $el->price }}</td>
                    <td>{{ $el->pic_count }}</td>
                    <td><a class="btn btn-default" href="/edit/{{ $el->id }}">Edit</a></td>
                    <td><a class="btn btn-default" href="/delete/{{ $el->id }}">Delete</a></td>
                </tr>
                @endforeach
            @endif
        </tbody>
    </table>
@endsection
