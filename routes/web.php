<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "IndexController@allData")
    ->name("home");

Route::get("/add", function() {
   return view("add");
})->name("add");

Route::get("/edit/{id}", "IndexController@edit")->name("edit");
Route::get("/delete/{id}", "IndexController@remove");

Route::post("/add/create", "IndexController@create")->name("add-form");
Route::post("/edit/editproduct", "IndexController@save")->name("save-form");
Route::post("/deletePic/{id}", "IndexController@deletepic");

